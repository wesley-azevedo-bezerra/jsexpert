const BaseRepository = require('./../repository/base/baseRepository')

class CarService{
    constructor({cars}){
        this.carRepository = new BaseRepository({file: cars})
    }

    async GetAvailableCar(carCategory){
      
        const carId = this.chooseRandomCar(carCategory)
        const car = await this.carRepository.find(carId)

        return car
    }

    getRandomPositionFromArray(list){
        const ListLength = list.length
        return Math.floor(
            Math.random() * (ListLength)
        ) 
    }


    chooseRandomCar(carCategory){
      
        const randomCarIndex = this.getRandomPositionFromArray(carCategory.carIds)
   
        const carId = carCategory.carIds[randomCarIndex]
        return carId

    }


}

module.exports = CarService